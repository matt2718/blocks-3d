{-# LANGUAGE DataKinds      #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE StaticPointers #-}

module Blocks.Blocks3d.Parity
  ( Parity (..)
  , parityFromIntegral
  , flipParity
  ) where

import Control.DeepSeq (NFData)
import Data.Aeson      (FromJSON (..), ToJSON (..))
import Data.Binary     (Binary)
import GHC.Generics    (Generic)
import Hyperion        (Dict (..), Static (..))

data Parity = ParityEven | ParityOdd
  deriving (Eq, Ord, Show, Bounded, Enum, Generic, Binary, NFData)

instance Static (Binary Parity) where closureDict = static Dict

parityToInt :: Parity -> Int
parityToInt ParityEven = 0
parityToInt ParityOdd  = 1

parityFromIntegral :: Integral a => a -> Parity
parityFromIntegral i = if even i then ParityEven else ParityOdd

instance ToJSON Parity where
  toJSON = toJSON . parityToInt

instance FromJSON Parity where
  parseJSON = fmap (parityFromIntegral @Int) . parseJSON

flipParity :: Parity -> Parity
flipParity ParityEven = ParityOdd
flipParity ParityOdd  = ParityEven
