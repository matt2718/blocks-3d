{-# LANGUAGE DeriveAnyClass      #-}
{-# LANGUAGE OverloadedRecordDot #-}
{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE RecordWildCards     #-}
{-# LANGUAGE StaticPointers      #-}

module Blocks.Blocks3d.WriteTable
  ( writeBlockTables
  , writeBlockTablesCustomLog
  , blockTableFilePath
  , groupBlockTableKeys
  , DebugLevel(..)
  ) where

import Blocks                        (coordinateToString)
import Blocks.Blocks3d.BlockTableKey (BlockTableKey' (..))
import Blocks.Blocks3d.LogConfig     (LogConfig, callProcessLogConfig,
                                      defaultLogConfig, logInfo)
import Blocks.Sign                   qualified as Sign
import Bootstrap.Math.HalfInteger    (HalfInteger)
import Bootstrap.Math.HalfInteger    qualified as HalfInteger
import Control.Monad                 (forM_, void)
import Data.Aeson                    (FromJSON, ToJSON)
import Data.Binary                   (Binary)
import Data.BinaryHash               (hashUntypedBase64Safe)
import Data.Foldable                 (foldMap')
import Data.List                     (intercalate)
import Data.Map.Strict               qualified as Map
import Data.Rendered                 (render)
import Data.Set                      qualified as Set
import Data.Text                     (Text)
import Data.Text                     qualified as Text
import Data.Text.IO                  qualified as Text
import GHC.Generics                  (Generic)
import Hyperion                      (Dict (..), Static (..))
import Hyperion.Util                 (randomString)
import System.Directory              (createDirectoryIfMissing, listDirectory,
                                      removeDirectory, renameFile)
import System.FilePath               ((<.>), (</>))

-- | The path for a block table takes the form
-- blocks_3d_tables_hash/spin_j.json, where "hash" is the hash of
-- BlockTableKey' with jInternal = (), and "j" is the value of
-- jInternal for that table (filled in by blocks_3d). Using the hash
-- has two disadvantages: (1) There can be collisions. This would be
-- very bad. (2) The filename doesn't look sensible to
-- humans. However, each file contains its own metadata, so this can
-- be overcome with tooling.
blockTableDir :: FilePath -> BlockTableKey' () -> FilePath
blockTableDir baseDir blockTable =
  baseDir </>  ("blocks_3d_tables_" ++ hashUntypedBase64Safe blockTable)

-- | File name for the given spin string.
mkSpinFileName :: String -> FilePath
mkSpinFileName spinString = "spin_" ++ spinString <.> "json"

-- | File name for the given spin
spinFileName :: HalfInteger -> FilePath
spinFileName = mkSpinFileName . showWithDecimal
  where
    -- | We use showWithDecimal for jInternal instead of show because
    -- blocks_3d outputs integers followed by ".0"
    showWithDecimal j = case HalfInteger.toInteger j of
      Just jInt -> show jInt ++ ".0"
      Nothing   -> show j

-- | Template for each spin file for blocks_3d
spinTemplateName :: FilePath
spinTemplateName = mkSpinFileName "{:.1f}"

-- | The file corresponding to a specific value of jInternal
blockTableFilePath :: FilePath -> BlockTableKey' HalfInteger -> FilePath
blockTableFilePath baseDir blockTable =
  blockTableDir baseDir (void blockTable) </> spinFileName blockTable.jInternal

data DebugLevel = NoDebug | Debug
  deriving (Eq, Ord, Show, Generic, Binary, FromJSON, ToJSON)

instance Static (Binary DebugLevel) where closureDict = static Dict

blockTableArgs :: Int -> DebugLevel -> FilePath -> BlockTableKey' (Set.Set HalfInteger) -> [(Text,Text)]
blockTableArgs numThreads debugLevel tableDir BlockTableKey{..} =
  [ ("j-external"     , showj4 jExternal)
  , ("j-internal"     , showjList (Set.toList jInternal))
  , ("j-12"           , showText j12)
  , ("j-43"           , showText j43)
  , ("delta-12"       , Text.pack $ render delta12)
  , ("delta-43"       , Text.pack $ render delta43)
  , ("delta-1-plus-2" , Text.pack $ render delta1Plus2)
  , ("four-pt-struct" , showj4 fourPtStruct)
  , ("four-pt-sign"   , showText (Sign.toNum @Int fourPtSign))
  , ("order"          , showText order)
  , ("lambda"         , showText lambda)
  , ("coordinates"    , Text.pack $ intercalate "," (map coordinateToString (Set.toList coordinates)))
  , ("kept-pole-order", showText keptPoleOrder)
  , ("num-threads"    , showText numThreads)
  , ("debug"          , case debugLevel of { NoDebug -> "0"; Debug -> "1" })
  , ("precision"      , showText precision)
  , ("out-template"   , Text.pack $ tableDir </> spinTemplateName)
  ]
  where
    showj4 (j1, j2, j3, j4) = Text.intercalate "," (map showText [j1,j2,j3,j4])
    showjList js = Text.intercalate "," (map showText js)

showText :: Show a => a -> Text
showText = Text.pack . show

writeIniFile :: FilePath -> [(Text,Text)] -> IO ()
writeIniFile file kvPairs =
  Text.writeFile file $ Text.unlines $ fmap mkPair kvPairs
  where
    mkPair (k,v) = k <> "=" <> v

-- | Run blocks_3d to generate the blocks corresponding to the given
-- BlockTableKey's. We additionally allow for a custom LogConfig,
-- since this may be needed by programs that generate many blocks in
-- parallel.
writeBlockTablesCustomLog
  :: LogConfig
  -> FilePath    -- ^ Path to the blocks_3d executable
  -> Int         -- ^ Number of threads to use
  -> DebugLevel  -- ^ Whether to print debug output
  -> FilePath    -- ^ Directory for block tables
  -> BlockTableKey' (Set.Set HalfInteger) -- ^ Block tables to generate
  -> IO ()
writeBlockTablesCustomLog logConfig blocks3dExecutable numThreads debugLevel baseDir b = do
  tmpString <- getTmpString
  let
    tableDir    = blockTableDir baseDir (void b)
    tableDirTmp = tableDir ++ tmpString
    -- We make a unique param file name so that we don't overwrite old
    -- param files, which might be needed for debugging
    paramFile = tableDirTmp </> "params" ++ tmpString <.> "ini"
  createDirectoryIfMissing True tableDir
  createDirectoryIfMissing True tableDirTmp
  writeIniFile paramFile $ blockTableArgs numThreads debugLevel tableDirTmp b
  logInfo logConfig "Writing Block3ds to temporary directory" (b, tableDirTmp)
  callProcessLogConfig logConfig blocks3dExecutable [ "--param-file", paramFile ]
  -- Move each file individually, instead of moving the directory, so
  -- that we can keep files that are already in the target location
  moveEachFileInDir tableDirTmp tableDir
  logInfo logConfig "Removing temporary directory" tableDirTmp
  removeDirectory tableDirTmp
  where
    getTmpString :: IO String
    getTmpString = do
      salt <- randomString 8
      pure $ "_tmp_" ++ salt

    moveEachFileInDir :: FilePath -> FilePath -> IO ()
    moveEachFileInDir fromDir toDir = do
      names <- listDirectory fromDir
      forM_ names $ \name -> do
        let
          fromFile = fromDir </> name
          toFile   = toDir   </> name
        logInfo logConfig "Moving file" (fromFile, toFile)
        renameFile fromFile toFile

-- | Run blocks_3d to generate the blocks corresponding to the given
-- BlockTableKey's. Uses 'defaultLogConfig'
writeBlockTables
  :: FilePath    -- ^ Path to the blocks_3d executable
  -> Int         -- ^ Number of threads to use
  -> DebugLevel  -- ^ Whether to print debug output
  -> FilePath    -- ^ Directory for block tables
  -> BlockTableKey' (Set.Set HalfInteger) -- ^ Block tables to generate
  -> IO ()
writeBlockTables = writeBlockTablesCustomLog defaultLogConfig

-- | Group keys by all their values except jInternal
newtype KeySet = KeySet (Map.Map (BlockTableKey' ()) (Set.Set HalfInteger))

-- | Union the jInternal values
instance Semigroup KeySet where
  (KeySet ks1) <> (KeySet ks2) = KeySet (Map.unionWith Set.union ks1 ks2)

instance Monoid KeySet where
  mempty = KeySet Map.empty

-- | Collect keys with different jInternal but the same values of all
-- other fields together into a single BlockTableKey' with a set of
-- jInternal's.
groupBlockTableKeys
  :: Foldable t
  => t (BlockTableKey' HalfInteger)
  -> [BlockTableKey' (Set.Set HalfInteger)]
groupBlockTableKeys = fromKeySet . foldMap' toKeySet
  where
    toKeySet k = KeySet (Map.singleton (void k) (Set.singleton (jInternal k)))
    fromKeySet (KeySet ks) = [fmap (const js) k | (k,js) <- Map.toList ks]
